from edgetpu.classification.engine import ClassificationEngine
from PIL import Image
import cv2
import re
import os
import argparse
from threading import Thread
import time
import traceback
from sensestorm import speak

detected_class = None
def sensestorm_action():
    global detected_class
    previous_class = None
    try:
        while True:
            current_class = detected_class

            # if class 0 has just been detected, but not detected previously
            if current_class == 0 and previous_class != 0:
                # do some actions
                speak("0")

            # if class 1 has just been detected, but not detected previously
            if current_class == 1 and previous_class != 1:
                # do some actions
                speak("1")

            #remember what was detected before
            previous_class = current_class

            # sleep a while in each loop to avoid exhausting CPU
            time.sleep(0.1)
    except:
        traceback.print_exc()
        os._exit(1)

Thread(target=sensestorm_action, args=(), daemon=True).start()

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--mirror",
    action='store_true',
    default=False,
	help="flip camera frame")

args = vars(ap.parse_args())
mirror = args['mirror']

# the TFLite converted to be used with edgetpu
modelPath = 'converted_edgetpu/model_edgetpu.tflite'

# The path to labels.txt that was downloaded with your model
labelPath = 'converted_edgetpu/labels.txt'

# This function parses the labels.txt and puts it in a python dictionary
def loadLabels(labelPath):
    p = re.compile(r'\s*(\d+)(.+)')
    with open(labelPath, 'r', encoding='utf-8') as labelFile:
        lines = (p.match(line).groups() for line in labelFile.readlines())
        return {int(num): text.strip() for num, text in lines}

# This function takes in a PIL Image from any source or path you choose
def classifyImage(image, engine):
    # image is reformated to a square to match training
    image.resize((224, 224))

    # Classify and ouptut inference
    classifications = engine.ClassifyWithImage(image)
    return classifications

font                   = cv2.FONT_HERSHEY_SIMPLEX
text_position               = (10,30)
fontScale              = 1
fontColor              = (255,255,255)
lineType               = 2

def main():
    global previous_class
    global detected_class

    # Load your model onto your Coral Edgetpu
    engine = ClassificationEngine(modelPath)
    labels = loadLabels(labelPath)

    cap = cv2.VideoCapture(0)
    while cap.isOpened():
        ret, frame = cap.read()
        # flip frame if camera is facing self
        if mirror:
            frame = cv2.flip(frame,1)

        if not ret:
            break

        # Format the image into a PIL Image so its compatable with Edge TPU
        cv2_im = frame
        pil_im = Image.fromarray(cv2_im)

        # Resize and flip image so its a square and matches training
        pil_im.resize((224, 224))
        pil_im.transpose(Image.FLIP_LEFT_RIGHT)

        # Classify and display image
        results = classifyImage(pil_im, engine)
        detected_class = results[0][0]

        cv2.putText(cv2_im,labels[results[0][0]] + ": " + str(results[0][1]), 
          text_position, 
          font, 
          fontScale,
          fontColor,
          lineType)

        cv2.imshow('frame', cv2_im)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()