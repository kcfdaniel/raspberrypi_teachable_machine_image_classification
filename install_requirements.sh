# install opencv
sudo apt-get update
sudo apt-get -y upgrade

sudo apt-get install -y libatlas-base-dev 

sudo apt-get install -y libjpeg-dev libtiff5-dev libjasper-dev libpng-dev

sudo apt-get install -y libqtgui4 libqtwebkit4 libqt4-test python3-pyqt5

sudo apt-get install -y libhdf5-dev libhdf5-serial-dev libhdf5-100 #for pi 3

sudo pip3 install opencv-contrib-python

sudo modprobe bcm2835-v4l2 #enable access to camera

# Add the Coral package repository to your apt-get distribution list by issuing the following commands:
echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update

# install edgetpu api 
sudo apt-get install -y python3-edgetpu

# install libedgetpu1-max for max performance, but hotter chip 
sudo apt-get install -y libedgetpu1-max

sudo reboot